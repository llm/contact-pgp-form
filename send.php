<?php
/* Très largement inspiré du script proposé par OpenPGP.JS
*/

// if the "subject" field is not empty, the user is a bot (as the field "subject" should be hidden from human (or cat) eyes
if (empty($_POST['subject']))
{
	/* setup -------------------------------------------------------------------- */
	define('SMALL_LIMIT',128); // chars limit for email
	define('LARGE_LIMIT',131072); // chars limit for text
	$subject="Contact sur Nom du site"; // Subject of the sent email
	$to="test@email.tld"; // Recipient email address
	$headers = "From: Contact Form <noreply@site.tld>\nContent-Type:text/html;charset=utf-8"; // email headers
	$redirect = "http://www.site.tld/contact.html"; // when the email is sent (or not), the user will be redirected here
	/* -------------------------------------------------------------------------- */

	$user=substr(trim($_POST["mail"]), 0, SMALL_LIMIT);
	$message=substr(trim($_POST["message"]), 0, LARGE_LIMIT);

	if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
		$headers = "From: $user";
	}
	if (!empty($message))
		mail($to, $subject, $message, $headers);

	header("Location: " . $redirect);
}
else
{
	// well, you know, if it's a bot...
	echo "Message envoyé (compte là dessus mon couillon).";
}
?>
